const express = require('express');
const router = express.Router();
const dataModel = require('../model/dataModel');

router.get('/get',async(req,res)=>{
    const fetchData =await dataModel.find()
    res.json(fetchData);
})

router.get('/get/:userid',async(req,res)=>{
    const filterData = await dataModel.find({_id : req.params.userid})
    res.json(filterData);
})

module.exports=router;