const express= require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
app.use(bodyParser.json());
app.use(cors());

//mongoose Connectivity
mongoose.connect('mongodb://localhost:27017/admin',{ useNewUrlParser: true, useUnifiedTopology: true });

// db connectivity check
const db = mongoose.connection;
db.once('open',()=>{
    console.log("MongoDB Connection Established")
})

app.route('/').get((req,res,next)=>{
    res.end("Recieving GET Route request");
})
.post((req,res,next)=>{
    res.end("Post Request acknowledged")
})
.put((req,res,next)=>{
    res.statusCode=200;
    res.end("Recived Put request ")
})
.delete((req,res,next)=>{
    res.statusCode=300;
    res.end("delete operation performed")
});

//router
const userRouter = require('./route/userRouter');
const promoRouter = require('./route/promoRouter');

app.use('/user', userRouter);
app.use('/promo' , promoRouter);


app.listen(3001);