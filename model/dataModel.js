const mongoose = require('mongoose');
const dataSchema = mongoose.Schema({
    user : {type:String , required : true},
    email : {type:String , required : true}
});

module.exports = mongoose.model('users',dataSchema);
